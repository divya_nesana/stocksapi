package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Stocks;
import com.example.demo.repository.StocksRepository;

@RestController
public class StocksController {

	@Autowired
	private StocksRepository stocksRepository;
	
	@GetMapping(path="/Stocks")
	public Stocks stocks(@RequestParam(required = false, defaultValue = "C") String ticker) {
		
		int volume = 0, stockStatus = 0;
		double price = 0.00;
		String buyOrSell = null;

		if (ticker.equals("C")) {
			volume = 36;
			price = 37542;
			stockStatus = 0;
			buyOrSell = "Buy";
		}
		
		Stocks stock = new Stocks(ticker, price, volume, buyOrSell, stockStatus);
		
		Stocks savedObject = stocksRepository.save(stock);
		
		return savedObject;
	}
	
	@GetMapping(path="/all")
	public @ResponseBody Iterable<Stocks> getAllUsers() {
	    // This returns a JSON or XML with the users
	    return stocksRepository.findAll();
	}
}
