package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Stocks;

public interface StocksRepository extends CrudRepository<Stocks, Long>{

}
